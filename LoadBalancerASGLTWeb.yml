AWSTemplateFormatVersion: '2010-09-09'
Parameters:
  SSHKeyName:
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instance
    Type: AWS::EC2::KeyPair::KeyName
    ConstraintDescription: must be the name of an existing EC2 KeyPair.
  
  Vpc:
    Description: VPC to deploy EC2 instance
    Type: AWS::EC2::VPC::Id

  ASGSubnetIds:
    Description: Subnets (atleast two) to deploy EC2 instances
    Type: List<AWS::EC2::Subnet::Id>

  ALBSubnetIds:
    Description: Public subnets (atleast two) to deploy ALB
    Type: List<AWS::EC2::Subnet::Id>

Resources:

  ALBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow HTTP port
      VpcId: !Ref Vpc
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 80
        ToPort: 80
        CidrIp: 0.0.0.0/0
      SecurityGroupEgress:
      - IpProtocol: -1
        CidrIp: 0.0.0.0/0
      Tags:
        - Key: Name
          Value: !Sub ${AWS::StackName}-my-ALB-SG

  EC2SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow HTTP port
      VpcId: !Ref Vpc
      SecurityGroupIngress:
      - IpProtocol: tcp
        FromPort: 80
        ToPort: 80
        CidrIp: 0.0.0.0/0
      SecurityGroupEgress:
      - IpProtocol: -1
        CidrIp: 0.0.0.0/0
      Tags:
        - Key: Name
          Value: !Sub ${AWS::StackName}-my-EC2-SG
  
  LaunchTemplate:
    Type: AWS::EC2::LaunchTemplate
    Properties:
      LaunchTemplateName: !Sub ${AWS::StackName}-my-launch-tp
      LaunchTemplateData:
        ImageId: ami-0cff7528ff583bf9a
        KeyName: !Ref SSHKeyName
        InstanceType: t2.micro
        IamInstanceProfile:
          Name: EC2InstanceRole
        UserData:
          Fn::Base64:
            !Sub |
              #!/bin/bash
              yum update -y
              yum install -y httpd.x86_64
              systemctl start httpd.service
              systemctl enable httpd.service
              curl -O https://bootstrap.pypa.io/get-pip.py
              python3 get-pip.py --user
              yum install git -y
              pip3 install git-remote-codecommit
              git clone codecommit::us-east-1://webcode-repo
              cp -r webcode-repo/* /var/www/html/
              yum install ruby -y
              yum install wget
              cd /home/ec2-user
              wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install
              chmod +x ./install
              ./install auto
              service codedeploy-agent status
        SecurityGroupIds:
        - !Ref EC2SecurityGroup
        TagSpecifications:
        - ResourceType: instance
          Tags:
          - Key: Name
            Value: !Sub ${AWS::StackName}-Instance
  
  ServerGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      VPCZoneIdentifier: !Ref ASGSubnetIds
      LaunchTemplate:
        LaunchTemplateId: !Ref LaunchTemplate
        Version: !GetAtt LaunchTemplate.LatestVersionNumber
      MaxSize: '3'
      MinSize: '1'
      DesiredCapacity: '2'
      TargetGroupARNs:
        - !Ref TargetGroup

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckEnabled: 'true'
      HealthCheckIntervalSeconds: 30
      HealthCheckPath: "/index.html"
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      Matcher:
        HttpCode: '200'
      Name: !Sub ${AWS::StackName}-my-Target-gp
      Port: 80
      Protocol: HTTP
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-my-Target-gp
      TargetGroupAttributes:
      - Key: deregistration_delay.timeout_seconds
        Value: '20'
      - Key: stickiness.enabled
        Value: 'true'
      TargetType: instance
      UnhealthyThresholdCount: 3
      VpcId:
        !Ref Vpc

  Listener80:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
      - TargetGroupArn:
          Fn::Join:
          - ''
          - - 'arn:aws:elasticloadbalancing:'
            - Ref: AWS::Region
            - ":"
            - Ref: AWS::AccountId
            - ":"
            - Fn::GetAtt:
              - TargetGroup
              - TargetGroupFullName
        Type: forward
      LoadBalancerArn:
        Fn::Join:
        - ''
        - - 'arn:aws:elasticloadbalancing:'
          - Ref: AWS::Region
          - ":"
          - Ref: AWS::AccountId
          - ":loadbalancer/"
          - Fn::GetAtt:
            - PublicALB
            - LoadBalancerFullName
      Port: '80'
      Protocol: HTTP

  PublicALB:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      IpAddressType: ipv4
      Name: !Sub ${AWS::StackName}-my-Pub-alb
      Scheme: internet-facing
      SecurityGroups:
      - Ref: ALBSecurityGroup
      Subnets: !Ref ALBSubnetIds
      Tags:
      - Key: Name
        Value: !Sub ${AWS::StackName}-my-Pub-alb
      Type: application

Outputs:
  ALBDNSName:
    Description: 'ALB Domain Name'
    Value: !GetAtt PublicALB.DNSName